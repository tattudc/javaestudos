package classes;

import java.util.Scanner;

public class jogo {

    private int resultado;
    private Dado dado1 = new Dado();
    private Dado dado2 = new Dado();
    private Jogador jogador1, jogador2;
    public Scanner entrada = new Scanner(System.in);

    public void inserirJogadores() {
        System.out.println("Jogador1, informe seu nome: ");
        jogador1 = new Jogador(entrada.next());
        System.out.println("Jogador2, Informe seu nome: ");
        jogador2 = new Jogador(entrada.next());
    }

    public void InserirApostas() {
        System.out.println(jogador1.getNome() + "Informe sua posta:");
        int aposta = 0;

        do {
            System.out.println(jogador1.getNome() + "Informe sua posta:");
            aposta = entrada.nextInt();

        } while (aposta < 2 || aposta > 12);
        jogador1.setValorAposta(aposta);

        do {
            System.out.println(jogador2.getNome() + "Informe sua posta:");
            aposta = entrada.nextInt();
            if (aposta == jogador1.getValorAposta()) {
                System.out.println("valor ja escolhido. Tente outro.");
            }
        } while (aposta < 2 || aposta > 12 || aposta == jogador2.getValorAposta());
        jogador2.setValorAposta(aposta);

    }

    public void JogarDados() {
        dado1.setValorFace();
        dado2.setValorFace();
    }

    public void MostrarResultado() {
        resultado = dado1.getValorFace() + dado2.getValorFace();
        System.out.println("Resultado: " + resultado);
    }

    public void MostrarVencedor() {
        if (resultado == jogador1.getValorAposta()) {
            System.out.println("JOgador1 venceu a aposta");
        } else if (resultado == jogador2.getValorAposta()) {
            System.out.println("JOgador2 venceu a aposta");
        } else {
            System.out.println("Computaodr Venceu");
        }
    }

}
